import axios from 'axios';

const liveUrl = 'https://api.pinpayments.com';
const testUrl = 'https://test-api.pinpayments.com';

var Pin = function(options) {

  const client = axios.post({
    prefixUrl: options.url,
    headers: { 'user-agent': 'pinjs (https://github.com/thomasdavis/pinjs)' },
    username: options.key,
    responseType: 'json'
  });

  this.createCard = function(fields, callback) {
    client.post('/1/cards', { json: fields })
      .then(response => callback(null, response, response.body))
      .catch(error => callback(error, error.response, error.response.body));
  };
};

var setup = function (options) {
  options.url = liveUrl;
  if(!options.production) {
    options.url = testUrl;
  }
  var pin = new Pin(options);
  return pin;
};

exports.setup = setup;
