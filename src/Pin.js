import axios from 'axios';
// import got from 'got';

// const liveUrl = 'https://api.pinpayments.com';
const testUrl = 'https://test-api.pinpayments.com';

const clientAxios = axios({
    prefixUrl: testUrl,
    headers: { 'user-agent': 'pinjs (https://github.com/thomasdavis/pinjs)' },
    username: 'pk_DSA-1XmSdIIViXp4bJO8SA',
    responseType: 'json'
});

export default class Pin {
    defaultMethod() {
        console.log('This is from Pin.js')
        return axios(testUrl, {
            headers: {
                'user-agent': 'pinjs (https://github.com/thomasdavis/pinjs)'
            },
            username: 'pk_DSA-1XmSdIIViXp4bJO8SA',
            responseType: 'json',
            method: 'get'
        });
    }

    createCard() {
        const client = axios.post(testUrl + '/1/cards', {
            // headers: {
            //     'user-agent': 'pinjs (https://github.com/thomasdavis/pinjs)'
            // },
            json: {
                number: 5520000000000000,
                expiry_month: '05',
                expiry_year: 2013,
                cvc: 519,
                name: 'Roland Robot',
                address_line1: '42 Sevenoaks St',
                address_city: 'Lathlain',
                address_postcode: 6454,
                address_state: 'WA',
                address_country: 'AU'
            },
            username: 'pk_DSA-1XmSdIIViXp4bJO8SA',
            responseType: 'json',
            // method: 'post'
        });
        this.clientAxiosFunc()
        return client
    }

    clientAxiosFunc() {
        console.log(clientAxios)
    }

    test() {
        return axios({
            url: "https://api.pinpayments.com/1/cards.json?callback=?",
            dataType: "jsonp",
            data: {
              method: 'POST',
              publishable_api_key: 'pk_DSA-1XmSdIIViXp4bJO8SA',
              // Other API parameters go here
            }
          });
    }

    test2() {
        return axios({
            url: "https://api.pinpayments.com/1/cards.json?callback=?",
            dataType: "jsonp",
            data: {
              _method: 'POST',
              publishable_api_key: 'pk_DSA-1XmSdIIViXp4bJO8SA',
              // Other API parameters go here
            }
          });
    }
}